on makeScreenshot(appName)
	set currentPath to POSIX path of (path to me)
	
	set ASTID to AppleScript's text item delimiters
	set AppleScript's text item delimiters to {"/"}
	set theContainer to text 1 thru text item -2 of currentPath & "/"
	set AppleScript's text item delimiters to ASTID
	
	set shellCommand to "/usr/sbin/screencapture " & (theContainer as string) & "ScreenShotManualActivationTestOf_" & appName & ".png"
	do shell script shellCommand
	
end makeScreenshot