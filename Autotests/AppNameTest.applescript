set screenshootLibrary to (load script ((path to me as string) & "::Screenshooter.scpt") as alias)
tell screenshootLibrary
	makeScreenshot("SuperRunner")
end tell
tell application "System Events" to tell application process "SuperRunner"
	activate
	set frontmost to true
	click menu item "About SuperRunner" of menu 1 of menu bar item "SuperRunner" of menu bar 1
	
	if static text "SuperRunner" of window 1 is (not (exists)) then do shell script "exit 1"
	
	click text field 1 of window "SuperRunner"
	
	# Get App Name
	set value of text field 1 of window "SuperRunner" to "test"
	
	# Close About Box
	click button "Button" of window "SuperRunner"
	
	set LabelValue to value of static text 1 of window "SuperRunner"
	
	if LabelValue is not "test" then do shell script "exit 1"
	return LabelValue
end tell

