//
//  test2.m
//  SuperRunner
//
//  Created by Viktor Radulov on 2/26/17.
//  Copyright © 2017 Viktor Radulov. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface test2 : XCTestCase

@end

@implementation test2

- (void)setUp {
    [super setUp];
    
    // Put setup code here. This method is called before the invocation of each test method in the class.

    // In UI tests it is usually best to stop immediately when a failure occurs.
    self.continueAfterFailure = NO;
    // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
    [[[XCUIApplication alloc] init] launch];

    // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample
{
	XCUIElement *superrunnerWindow = [[XCUIApplication alloc] init].windows[@"SuperRunner"];
	XCUIElement *fieldTextField = superrunnerWindow.textFields[@"Field"];
	[fieldTextField click];
	[fieldTextField typeText:@"test2"];
	[superrunnerWindow.buttons[@"Button"] click];
	XCTAssert([superrunnerWindow.staticTexts[@"Label"].value isEqualToString:@"test2"]);
}

@end
