//
//  SuperRunnerUITests.m
//  SuperRunnerUITests
//
//  Created by Viktor Radulov on 2/26/17.
//  Copyright © 2017 Viktor Radulov. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface SuperRunnerUITests : XCTestCase

@end

@implementation SuperRunnerUITests

- (void)setUp
{
    [super setUp];
    self.continueAfterFailure = NO;
	
    [[[XCUIApplication alloc] init] launch];
	
}

- (void)tearDown
{

    [super tearDown];
}

- (void)testExample
{
	XCUIElement *superrunnerWindow = [[XCUIApplication alloc] init].windows[@"SuperRunner"];
	XCUIElement *fieldTextField = superrunnerWindow.textFields[@"Field"];
	[fieldTextField click];
	[fieldTextField typeText:@"test"];
	[superrunnerWindow.buttons[@"Button"] click];
	XCTAssert([superrunnerWindow.staticTexts[@"Label"].value isEqualToString:@"test"]);
}

@end
