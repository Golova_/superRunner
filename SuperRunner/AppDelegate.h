//
//  AppDelegate.h
//  SuperRunner
//
//  Created by Viktor Radulov on 2/26/17.
//  Copyright © 2017 Viktor Radulov. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>


@end

