//
//  main.m
//  SuperRunner
//
//  Created by Viktor Radulov on 2/26/17.
//  Copyright © 2017 Viktor Radulov. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
	return NSApplicationMain(argc, argv);
}
