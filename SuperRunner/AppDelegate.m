//
//  AppDelegate.m
//  SuperRunner
//
//  Created by Viktor Radulov on 2/26/17.
//  Copyright © 2017 Viktor Radulov. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@property (assign) IBOutlet NSWindow *window;

@property (assign) IBOutlet NSTextField *field;
@property (assign) IBOutlet NSTextField *label;

@property (nonatomic, retain) NSArray *array;

@end

@implementation AppDelegate

- (void)dealloc
{
	[_array release];
	[super dealloc];
}

- (NSArray *)array
{
	return @[@"Hello", @"Hello", @"Hello", @"Hello",];
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
	
}


- (void)applicationWillTerminate:(NSNotification *)aNotification
{
	
}

- (IBAction)action:(id)sender
{
	self.label.stringValue = self.field.stringValue;
}

@end
